

# 第一章 RISC-V openEuler简介 

## 实验一 使用QEMU安装RISC-V openEuler

### 1. 编译支持视频输出的QEMU

#### 1.1 Ubuntu 20.04上基于源码安装qemu-rv64

##### 1.1.1 通过QEMU源代码构建

- 安装必要的构建工具

```bash
sudo apt install build-essential autoconf automake autotools-dev pkg-config bc curl gawk git bison flex texinfo gperf libtool patchutils mingw-w64 libmpc-dev libmpfr-dev libgmp-dev libexpat-dev libfdt-dev zlib1g-dev libglib2.0-dev libpixman-1-dev libncurses5-dev libncursesw5-dev meson libvirglrenderer-dev libsdl2-dev -y
sudo add-apt-repository ppa:deadsnakes/ppa
sudo apt install python3.9 python3-pip  -y
sudo apt install -f
pip3 install meson
```

-  下载支持视频输出QEMU源码包方法1（2选1）

注：如下载连接超时请重试几遍

```bash
git clone -b display https://gitlab.com/wangjunqiang/qemu.git
```

```bash
cd qemu
git submodule init
git submodule update --recursive
mkdir build
cd build
```

- 下载支持视频输出QEMU源码包方法2（2选1）

```bash
wget https://download.qemu.org/qemu-7.0.0.tar.xz
tar xvJf qemu-7.0.0.tar.xz
cd qemu-7.0.0
mkdir build
cd build
```

- 配置riscv64-qemu

以下命令中`xbot`为用户目录名

```bash
../configure  --enable-kvm --enable-sdl --enable-gtk --enable-virglrenderer --enable-opengl --target-list=riscv64-softmmu,riscv64-linux-user --prefix=/home/xbot/program/riscv64-qemu
```

`riscv-64-linux-user`为用户模式，可以运行基于 RISC-V 指令集编译的程序文件, `softmmu`为镜像模拟器，可以运行基于 RISC-V 指令集编译的Linux镜像，为了测试方便，可以两个都安装

- 编译

```bash
make -j $(nproc)
make install
```

如果 `--prefix` 指定的目录位于根目录下，则需要在 `./configure` 前加入 `sudo`

##### 1.1.2 配置环境变量

在环境变量PATH中添加riscv64-qemu所在目录，使相关命令可以直接使用

```bash
vim ~/.bashrc
```

`~/.bashrc`文末添加

````bash
export QEMU_HOME=/home/xbot/program/riscv64-qemu
export PATH=$QEMU_HOME/bin:$PATH
````

**注意一定要将 `QEMU_HOME` 路径替换为 `--prefix` 定义的路径**

检查是否添加成功

```bash
source ~/.bashrc
echo $PATH
```
屏幕回显包含`/home/xbot/program/riscv64-qemu`

```bash
/home/xbot/program/riscv64-qemu/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/snap/bin
```

##### 1.1.3 验证安装是否正确

```bash
qemu-system-riscv64 --version
```

如出现类似如下输出表示 QEMU 工作正常

```bash
QEMU emulator version 6.2.90 (v7.0.0-rc0-40-g2058fdbe81)
Copyright (c) 2003-2022 Fabrice Bellard and the QEMU Project developers
```

或

```bash
QEMU emulator version 7.0.0
Copyright (c) 2003-2022 Fabrice Bellard and the QEMU Project developers
```

#### 1.2 Ubuntu 22.04直接使用apt安装qemu

```bash
sudo apt install qemu-system-riscv64 -y
```

### 2. 系统镜像的使用

#### 2.1 镜像下载

##### 2.1.1 下载内容

- 下载 QEMU 目录下的[openeuler-qemu-xfce.raw.tar.zst](https://mirror.iscas.ac.cn/openeuler-sig-riscv/openEuler-RISC-V/testing/20220621/v0.1/QEMU/openeuler-qemu-xfce.raw.tar.zst)、[fw_payload_oe_qemuvirt.elf](https://mirror.iscas.ac.cn/openeuler-sig-riscv/openEuler-RISC-V/testing/20220621/v0.1/QEMU/fw_payload_oe_qemuvirt.elf) 和 [start_vm_xfce.sh](https://mirror.iscas.ac.cn/openeuler-sig-riscv/openEuler-RISC-V/testing/20220621/v0.1/QEMU/start_vm_xfce.sh)
- 下载地址 [https://mirror.iscas.ac.cn/openeuler-sig-riscv/openEuler-RISC-V/testing/20220621/v0.1/QEMU/](https://mirror.iscas.ac.cn/openeuler-sig-riscv/openEuler-RISC-V/testing/20220621/v0.1/QEMU/)

##### 2.1.2 部署和启动

- 解压 tar.zst 格式的镜像文件

注：解压需要10.7G硬盘空间

```bash
sudo apt install zstd -y
tar -I zstdmt -xvf ./openeuler-qemu-xfce.raw.tar.zst
```

- 执行 `bash start_vm_xfce.sh`

注：QEMU下启动Xfce较慢，请耐心等待

- 输入密码完成登录，默认的用户名和密码为 `root` 和 `openEuler12#$`

### 3.  配置openEuler

#### 3.1 修改密码

当系统启动之后，可以在 Host 上通过 ssh 登录到运行于 QEMU 模拟器中的 openEuler OS：

```bash
$ ssh -p 12055 root@localhost
```

root 默认密码为 openEuler12#$

为 root 设置密码

```bash
passwd root
Password:
Retype:
```

#### 3.2 添加用户

添加用户`euler`并设置密码

```bash
adduser -d /home/euler -s /bin/bash euler
passwd euler
Password:
Retype:
```

`su` 是 Switch User 的命令，用于切换用户。

从用户`root`切换到用户`euler`执行 `su euler`，一并切换到`euler`的主目录则执行 `su - euler`

从用户`root`切换到用户`euler`执行 `su `，一并切换到`root`的主目录则执行 `su -`

#### 3.3 磁盘扩容

在提高容量上限以前，我们可以先检查一下 qcow2 文件：

``` bash
qemu-img info *.qcow2
```

屏幕回显:

```
image: openEuler-preview.riscv64.qcow2
file format: qcow2
virtual size: 10 GiB (10737418240 bytes)
disk size: 9.58 GiB
cluster_size: 65536
Format specific information:
    compat: 1.1
    compression type: zlib
    lazy refcounts: false
    refcount bits: 16
    corrupt: false
    extended l2: false
```

可以看到，disk size 快接近 virtual size 了。

然后我们把 qcow2 文件的容量上限提高到 200 GB：

```bash
qemu-img resize *.qcow2 +190G
```

屏幕回显:

```
Image resized.
```

```bash
qemu-img info *.qcow2
```

屏幕回显

```
image: openEuler-preview.riscv64.qcow2
file format: qcow2
virtual size: 200 GiB (214748364800 bytes)
disk size: 9.58 GiB
cluster_size: 65536
Format specific information:
    compat: 1.1
    compression type: zlib
    lazy refcounts: false
    refcount bits: 16
    corrupt: false
    extended l2: false
```

可以看到，现在 virtual size 变成了 200 GB。

然后通过 QEMU 启动 openEuler RISC-V。

启动以后，我们先看看分区情况：

```bash
lsblk
```

屏幕回显:
```
NAME   MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
vda    254:0    0  200G  0 disk
└─vda1 254:1    0   10G  0 part /
```

可以看到根目录对应的分区只使用了 10G。

接下来通过 `dnf install cloud-utils-growpart` 安装这个提供 `growpart` 的软件包。在安装过程中，dnf 可能会提示空间不够，这时需要删除一些没用的文件，比如 osc 下载下来的缓存软件包。

安装好以后，用 `growpart` 扩展分区 vda1：

```bash
growpart /dev/vda 1
```

屏幕回显:

```
CHANGED: partition=1 start=2048 old: size=20969472 end=20971520 new: size=419428319 end=419430367
[root@openEuler-RISCV-rare ~]# lsblk
NAME   MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
vda    254:0    0  200G  0 disk
└─vda1 254:1    0  200G  0 part /
```

可以看到，vda1 分区的已经扩展到了 200G。

最后对文件系统进行一下调整就可以了：

```bash
[root@openEuler-RISCV-rare ~]# resize2fs /dev/vda1
```

屏幕回显:

```
resize2fs 1.45.3 (14-Jul-2019)
Filesystem at /dev/vda1 is mounted on /; on-line resizing required
old_desc_blocks = 2, new_desc_blocks = 25
The filesystem on /dev/vda1 is now 52428539 (4k) blocks long.
```

### 4. RISC-V系统验证

1. 安装 vim

```bash
dnf install vim
```

2. 使用 vim 编写一个简单的 c 程序

```bash
vim hello.c
```

输入

```c
#include <stdio.h>

int main(int argc, char *argv[])
{
        printf("Hello RISC-V! \n");
        return 0;
}
```

3. 编译hello.c

```bash
gcc hello.c -o hello
```

检查编译后的文件格式

```bash
file hello

hello: ELF 64-bit LSB pie executable, UCB RISC-V, version 1 (SYSV), dynamically linked, interpreter /lib/ld-linux-riscv64-lp64d.so.1, BuildID[sha1]=4a75c57e4e99654dca0d6dc91689dffbbe7dc581, for GNU/Linux 4.15.0, not stripped
```
看到是 RISC-V 的 elf 文件就说明 gcc 正常 运行 hello

```bash
./hello
Hello RISC-V!
```

参考文献:

https://computingforgeeks.com/how-to-extend-increase-kvm-virtual-machine-disk-size/

https://computingforgeeks.com/resize-ext-and-xfs-root-partition-without-lvm/