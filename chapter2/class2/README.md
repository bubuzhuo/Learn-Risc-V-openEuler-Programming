# 第二章 openEuler下C语言开发与调试
## 第二讲 GCC

### 1. GCC的安装

```bash
dnf install gcc
```

### 2. GCC编译过程

**机器语言，汇编语言和高级语言**

- 机器语言：机器语言是用二进制代码表示的计算机能直接识别和执行的一种机器指令的集合。它是计算机的设计者通过计算机的硬件结构赋予计算机的操作功能。

  不同型号计算机之间机器语言是不相通的。

- 汇编语言：汇编语言是直接面向处理器（Processor）的程序设计语言。处理器是在指令的控制下工作的，处理器可以识别的每一条指令称为机器指令。

- 高级语言：计算机语言具有高级语言和低级语言之分。而高级语言又主要是相对于汇编语言而言的，它是较接近自然语言和数学公式的编程，基本脱离了机器的硬件系统，用人们更易理解的方式编写程序。编写的程序称之为源程序。如C，Java，Python等就是高级语言

我们平时编写的程序大都是使用高级语言编写的，想要得到可执行文件，必须将写好的程序进行编译，使用IDE工具的化可点击编译选项（可能是锤子图标播放图标等），编译的过程是怎样的呢？

### 2.1 C文件的编译过程

- 预处理，一般是包含头文件，展开宏定义，处理注释等，生成.i格式输出文件编译，将预处理后的文件编译生成汇编代码 
- 编译，把预处理完的文件进行一系列的词法分析，语法分析，语义分析及优化后生成相应的汇编代码。
- 汇编，将汇编代码转变成机器可以执行的命令
- 链接，将多个不同的目标文件以及动态库和静态库路径链接生成最终的可执行文件

这里我们使用一个简单的“hello world“的c程序演示。

### 2.2 预处理

- 将所有的#define删除，并且展开所有的宏定义
- 处理所有的条件预编译指令，比如#if #ifdef #elif #else #endif等
- 处理#include 预编译指令，将被包含的文件插入到该预编译指令的位置。
- 删除所有注释 “//”和”/* */”.
- 添加行号和文件标识，以便编译时产生调试用的行号及编译错误警告行号。
- 保留所有的#pragma编译器指令以备编译器使用

通常使用以下命令来进行预处理

```bash
gcc -E hello.c -o hello.i
```

参数-E表示只进行预处理，打开hello.i就可以看到预处理完成的内容。

查看文件:

```bash
file hello.i
hello.i: C source, ASCII text
```

可以看到预编译后的文件依然是ASCII文本文件。

### 2.3 编译

以下例子输出使用x86 ISA为例。

编译过程就是把预处理完的文件进行一系列的词法分析，语法分析，语义分析及优化后生成相应的汇编代码。预处理之后，可直接对生成的hello.i文件编译，生成汇编代码。

```bash
gcc -S hello.i -o hello.s
```

gcc的-S选项，表示在程序编译期间，在生成汇编代码后，停止，-o输出汇编代码文件

查看文件：

```bash
file hello.s
hello.s: assembler source, ASCII text
```

hello.s是汇编文件。你也直接打开hello.s文件查看汇编代码，如下

```assembly
    .file   "test.c"
    .section    .rodata
.LC0:
    .string "a=%d, b=%d, a+b=%d\n"
    .text
    .globl  main
    .type   main, @function
main:
.LFB0:
    .cfi_startproc
    pushl   %ebp
    .cfi_def_cfa_offset 8
    .cfi_offset 5, -8
    movl    %esp, %ebp
    .cfi_def_cfa_register 5
    andl    $-16, %esp
    subl    $32, %esp
    movl    $2, 20(%esp)
    movl    $3, 24(%esp)
    movl    24(%esp), %eax
    movl    %eax, 4(%esp)
    movl    20(%esp), %eax
    movl    %eax, (%esp)
    call    add 
    movl    %eax, 28(%esp)
    movl    28(%esp), %eax
    movl    %eax, 12(%esp)
    movl    24(%esp), %eax
    movl    %eax, 8(%esp)
    movl    20(%esp), %eax
    movl    %eax, 4(%esp)
    movl    $.LC0, (%esp)
    call    printf
    leave
    .cfi_restore 5
    .cfi_def_cfa 4, 4
    ret 
    .cfi_endproc
.LFE0:
    .size   main, .-main
    .ident  "GCC: (Ubuntu 4.8.2-19ubuntu1) 4.8.2"
    .section    .note.GNU-stack,"",@progbits
```

### 2.4 汇编

汇编器是将汇编代码转变成机器可以执行的命令，每一个汇编语句几乎都对应一条机器指令。汇编相对于编译过程比较简单，根据汇编指令和机器指令的对照表一一翻译即可。

```bash
gcc -c hello.s -o hello.o
```

汇编hello.s生成目标文件hello.o

查看文件：

```shell
file hello.o
hello.o: ELF 64-bit LSB relocatable, x86-64, version 1 (SYSV), not stripped
```

ELF是Executable and Linkable file的缩写，说明它是一个可执行且可链接的文件，并且可以重定位。

### 2.5 链接
gcc连接器是gas提供的，负责将程序的目标文件与所需的所有附加的目标文件连接起来，最终生成可执行文件。附加的目标文件包括静态连接库和动态连接库。把汇编生成的hello.o，将其与Ｃ标准输入输出库进行连接，最终生成程序hello

```shell
gcc hello.o -o hello
```

**完成**
执行`./hello`可以向屏幕输出
HelloWorld!

```mermaid
graph TD
C语言源文件-->|预编译,生成hello.i|A(预编译后的文件)
A-->|编译,生成hello.s|B(汇编文件)
B-->|汇编,生成hello.o|C(目标文件)
C-->|链接,生成hello|可执行文件
```

## 2. GCC基本命令

**使用格式**

```bash
$ gcc  [ 选项 ]   [文件名]
```

常用选项

| 选项               | 含义                                                         |
| ------------------ | ------------------------------------------------------------ |
| -o                 | 将经过gcc处理过的结果存为文件file,这个结果文件可能是预处理文件、汇编文件、目标文件或者最终的可执行文件。假设被处理的源文件为source. suffix,如果这个选项被省略了，那么生成的可执行文件默认名称为a.(out），目标文件默认名为source. o;汇编文件默认名为source. s;生成的预处理文件则发送到标准输出设备。 |
| -c                 | 只编译，不链接生成可执行文件，编译器只是由输入的.c等源代码文件生成.o为后缀的目标文件。在对源文件进行差错时，或者需产生目标文件时可以使用该选项。 |
| -g                 | 在可执行文件中加入调试信息，方便进行程序的调试。如果使用中的括号中的选项，表示加入gdb扩展的调试信息，方便使用gdb来进行调试。 |
| -O[0,1,2,3]        | 在可执行文件中加入调试信息，方便进行程序的调试。如果使用中括号中的选项，表示加入gdb扩展的调试信息，方便使用gdb来进行调试对生成的代码使用优化，中括号中的部分为优化级缺省的情况为2级优化，0为不进行优化。注意，采用更高级的优化并不一定得到效率更高的代码。 |
| -I dirname         | 将dirname所指出的目录加入到程序头文件目录列表中，是在预编译过程中使用的参数。 |
| -L dirname         | 将dirname所指出的目录加入到程序函数档案库文件的目录列表中，是在连接过程中使用的参数。在预设状态下，连接程序id在系统的预设路径中（如/usr/lib）寻找所需要的档案库文件，这个选项告诉连接程序，首先找到-L指定的目录中取寻找，然后到系统预设路径中寻找，如果函数库存放在多个目录下，就需要一次使用这个选项，给出响应的存放目录。 |
| -lname             | 在连接是，装载名字为‘libname.a’的函数库，该函数库位于系统预设的目录或者由-L选项确定的目录下。例如，-lm表示连接名为‘libm.a’的数学函数库。 |
| -w                 | 禁止所有警告                                                 |
| -W warning         | 允许产生warning类型的警告，warning可以使：mian,unused等很多取值，最常用的是-Wall，表示产生所有警告。如果warning取值为error，其含义是将所有的警告作为错误（error），即出现警告就停止编译。 |
| -pedantic[-errors] | 表示gcc只发出ANSI/ISO C标准列出的所有警告，-pendantic -errors 仅仅针对错误。 |
| -ansi              | 支持ANSI/ISO C 的标准语法，取消GNU的语法中与该标准有冲突的部分，但并不保证生成与ANSI兼容的代码。 |

例子：编写打印”hello，world“的main.c程序。

```c
#include <stdio.h>
int main()
{
	printf("Hello World!\n");
	return 0;
}
```

-o选项：输出指定名称的可执行文件

```bash
gcc main.c -o hello
```

生成可执行文件hello。

运行：

```bash
./hello

Hello World!
```

```bash
gcc -o hello1 main.c
```

上面命令编译main.c文件产生可执行文件hello1。这个hello1文件与之前的hello文件内容是相同的。main.c文件和hello1两个顺序不会影响输出。

```bash
gcc mian.c
```

上面命令没有-o选项，没有指定可执行文件的名称，输出的可执行文件为`./a.out`。

```bash
ls

a.out   main.c
```

发现生成了默认的a.out文件，运行结果同hello。

```bash
./a.out

Hello World!
```

实际上，在很多gcc编译选项之后都可以加上-o选项，然后在-o选项之后加上想要生成的文件名称，就可以生成对应的文件。

例子

```shell
gcc -c main.c -o hello.o
```

编译main.c，生成目标文件hello.o。

**-c选项：**

```shell
gcc -c main.c
```

查看结果

```shell
ls

main.c  main.o
```

产生了目标文件main.o

查看main.o文件：

```bash
file main.o

main.o: ELF 64-bit LSB relocatable, x86-64, version 1 (SYSV), not stripped
```

ELF表示`executable and linkable file`表示他可执行并可链接，实际上，我们`-c`选项已经生了可执行文件（但是没有权限运行），只是没有链接。只需要在进一步链接就可执行了。

**-g选项：**

```bash
gcc -g main.c -o hi
```

加入调试信息，然后可以打开gdb debug

```bash
gdb hi

GNU gdb (Ubuntu 7.11.1-0ubuntu1~16.5) 7.11.1
Copyright (C) 2016 Free Software Foundation, Inc.
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.  Type "show copying"
and "show warranty" for details.
This GDB was configured as "x86_64-linux-gnu".
Type "show configuration" for configuration details.
For bug reporting instructions, please see:
<http://www.gnu.org/software/gdb/bugs/>.
Find the GDB manual and other documentation resources online at:
<http://www.gnu.org/software/gdb/documentation/>.
For help, type "help".
Type "apropos word" to search for commands related to "word"...
Reading symbols from hi...done.
(gdb) run
Starting program: /home/leiyunfei/playground/demo1/hi 
Hello World!
[Inferior 1 (process 11501) exited normally]
```

**-O[0,1,2,3]选项：**

设置优化等级，等级越高并不意味者结果代码效果越好。

```bash
gcc -O3 main.c 
```

**链接过程和库：**

- 链接：

我们将hello world分开分成两块代码和一个头文件。

hello.c

```c
#include<stdio.h>
#include"hello.h"
  
void hello(const char* name)
{
        printf("Hello %s!\n",name);
}
```

这块代码只包含一个函数hello，这个函数的作用是打印hello+一个输入的参数。注意这里调用了头文件hello.h。

main.c

```c
#include"hello.h"

int main()
{
        hello("World");
        return 0;
}
```

这个程序包含主函数，主函数调用hello函数，也调用了头文件hello.h。

头文件hello.h

```c
#ifndef LZ_HELLO_H_
#define LZ_HELLO_H_

void hello(const char* name);

#endif
```

头文件里声明了函数hello。

在这种情况下我们该如何编译呢？

```bash
gcc hello.c main.c -o hello
```

可以得到可执行文件hello。为什么可以这样这就要知道链接的定义。链接过程将多个目标文以及所需的库文件(.a,.so等)链接成最终的可执行文件(executable file)。比如我们调用了printf函数，它的内容在标准库stdio中，我们的代码中并没有它的定义，但是经过将分别编译完成的目标文件和库文件链接在一起就可以生成最后的可执行文件，而目标文件的个数不一定唯一。

```mermaid
graph TD
A(.o)-->D(Linker)
B(.a)-->D
C(.so)-->D
E(.o)-->D
```

动态链接库和静态链接库

上面的例子使用的是静态链接库，静态链接库和动态链接库两个名称来自链接时采用的是静态链接方法还是动态链接方法。两种方法的区别在于库函数是否被编译到程序内部。静态链接库，顾名思义，它是被编译到程序内部的，动态链接库是当程序需要的时候才会去访问的。

静态链接库的后缀为.a，动态链接库的后缀是.so。

**-I dirname 选项：**

指定头文件目录。

main.c

```c
#include<stdio.h>
#include"header.h"

int main()
{
        int i = NUM;
        printf("Hello World!\n");
        printf("Now, I am %d years old.\n",i);
        return 0;
}
```

我们引用了头文件header.h，我们没有这个头文件，在home目录下创建文件夹lib

````bash
mkdir lib
````

然后在文件夹中创建header.h头文件

header.h

```c
#define NUM 5
```

这样就可以使用-I选项了

```bash
gcc -I ~/lib main.c -o hi
```

这是告诉gcc在哪个文件夹下寻找需要的库文件。

**-L选项和-l选项（大小写l，L）：**

-L：指定链接库目录。

-l：指定库的名字。比如-lmath,（注意这里l后跟的苦命是省去lib和后缀.a,.so的）。

例子

```bash
gcc main.c -lmath -o hi
```

表示包括库libmath.so（也可以是静态库）进行编译。

```bash
gcc main.c -Llibs -lm -o hi
```

**-Wall选项：**

修改程序main.c

```c
#include<stdio.h>
int main()
{
	printf("Hello World!\n");
	printf("Now, I am %d years old.\n",21.);
	return 0;
}
```

加入一条信息，打印年龄，%d，但是后面跟的是浮点型21. 这样的话虽然编译可以通过，但实际上程序是有问题的，程序的输出也是不对的：

```bash
./hi

Hello World!
Now, I am 22024208 years old.
```

并不是21岁。这个时候加入-Wall选项就是必要的，打印警告信息。

```shell
gcc -Wall main.c -o hi
```

```shell
gcc -Wall main.c -o hi

main.c: In function ‘main’:
main.c:5:9: warning: format ‘%d’ expects argument of type ‘int’, but argument 2 has type ‘double’ [-Wformat=]
  printf("Now, I am %d years old.\n",21.);
         ^
```

它会将警告的信息显示出来。`Wall`选项一般推荐任何时候都要加上，因为这样可以减少很多程序不应该有的错误。

**检错选项**

```bash
gcc -Wall illcode.c -o illcode
```

- Wall选项，能够使GCC产生尽可能多的警告信息

多个程序文件的编译，为了方便代码重用，通常将主函数和其他函数放在不同文件中的方法
每个函数都有函数声明（函数头）和函数实现（函数体）两部分组成，函数头一般放在头文件中（*.h）中，而函数的定义文件放在实现文件（*.c、*.cpp)中

假设有一个由test1.c和 test2.c两个源文件组成的程序，对它们进行编译，并最终生成可执行程序test，可以使用下面这条命令

```bash
gcc test1.c test2.c -o test
```

事实上，上面这条命令相当于依次执行如下三条命令：

```bash
gcc -c test1.c -o test1.o
gcc -c test2.c -o test2.o
gcc test1.o test2.o -o test
```

**练习1**

greeting.h

```c
#ifndef GREETING_H
#define GREETING_H

void greeting(char *name);

#endif
```

greeting.c

```c
#include<stdio.h>
#include"greeting.h"

void greeting(char *name)
{
  printf("Hello %s!\r\n",name); 
}
```

my_app.c

```c
#include <stdio.h>
#include "greeting.h"
#define N 10

int main(void)
{
    char name[N];
    printf("Your name,Please:");
    scanf("%s",name);
    greeting(name);
    return 0;
}
```

要求在如下的目录结构下编译产生可执行文件。

**目录结构(1)**

```mermaid
graph LR
./-->greeting.h
./-->greeting.c
./-->my_app.c
```

编译

```bash
gcc  my_app.c  greeting.c  –o  my_app
```

**目录结构(2)**

```mermaid
graph LR
A(./)-->B(function)
A-->greeting.h
A-->my_app.c
B-->greeting.c
```

编译

```bash
gcc my_app.c function/greeting.c -I ./ -o my_app
```

**目录结构(3)**

```mermaid
graph LR
./-->A(functions)
./-->my_app.c
A-->greeting.h
A-->greeting.c
```

**编译方式(1)**

```bash
gcc my_app.c  functions/greeting.c  –o my_app  -I functions
```

**编译方式(2)**
分步编译

```shell
gcc  -c  my_app.c  -I function
gcc  -c  functions/greeting.c
gcc  my_app.o  greeting.o  –o my_app
```

思路：编译每一个.c文件，得到.o的目标文件；

**练习2**

目录结构

```mermaid
graph LR
A(./)-->B(function)
A-->main.c
A-->hello.h
B-->hello.c
```

要求在`/function`目录下分别产生链接库`libhello.a`和`libhello.so`到lib文件夹下，然后利用main.c和链接库进行编译。

产生链接库：

生成链接库需要目标文件，所以第一步先生成目标文件：

```bash
gcc -c hello.c -o hello.o -I..
```

然后

```bash
ar -rc libhello.a hello.o
```

生成静态库libhello.a。

```bash
mv libhello.a ../lib
```

移动`libhello.a`到lib文件夹下。

编译

```bash
gcc main.c -o hello -L./lib -lhello 
```

使用C++编程的同学建议参考[CppCoreGuidelines](https://github.com/isocpp/CppCoreGuidelines/blob/master/CppCoreGuidelines.md)

