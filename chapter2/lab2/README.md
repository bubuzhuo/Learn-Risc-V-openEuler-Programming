# 第二章 openEuler下C语言开发与调试

## 实验二 Makefile的使用

参考答案见[code子目录](./code)

### 1. 子任务1：单个源文件的Makefile的书写

#### 1.1 相关知识

 Linux 环境下的程序员如果不会使用GNU make来构建和管理自己的工程，应该不能算是一个合格的专业程序员，至少不能称得上是 Unix程序员。在Linux或Unix环境下，对于只含有几个源代码文件的小程序（如hello.c）的编译，可以手工键入gcc命令对源代码文件逐个进行编译；然而在大型的项目开发中，可能涉及几十到几百个源文件，采用手工键入的方式进行编译，则非常不方便，而且一旦修改了源代码，尤其头文件发生了的修改，采用手工方式进行编译和维护的工作量相当大，而且容易出错。所以在Linux或Unix环境下，人们通常利用GNU make工具来自动完成应用程序的维护和编译工作。在 Linux（unix ）环境下使用GNU 的make工具能够比较容易的构建一个属于你自己的工程，整个工程的编译只需要一个命令就可以完成编译、连接以至于最后的执行。不过这需要我们投入一些时间去完成一个或者多个称之为Makefile 文件的编写。

Makefile是按照某种脚本语法编写的文本文件，而GNU make能够对Makefile中指令进行解释并执行编译操作。Makefile文件定义了一系列的规则来指定哪些文件需要先编译，哪些文件需要后编译，哪些文件需要重新编译，甚至于进行更复杂的功能操作。所要完成的Makefile 文件描述了整个工程的编译、连接等规则。其中包括：工程中的哪些源文件需要编译以及如何编译、需要创建那些库文件以及如何创建这些库文件、如何最后产生我们想要的可执行文件。尽管看起来可能是很复杂的事情，但是为工程编写Makefile 的好处是能够使用一行命令来完成“自动化编译”，一旦提供一个（通常对于一个工程来说会是多个）正确的 Makefile。编译整个工程你所要做的唯一的一件事就是在shell 提示符下输入make命令。整个工程完全自动编译，极大提高了效率。

make命令执行时，需要一个 Makefile 文件，以告诉make命令需要怎么样的去编译和链接程序。我们要写一个Makefile来告诉make命令如何编译和链接文件。我们的规则是：

1. 如果这个工程没有编译过，那么我们的所有C文件都要编译并被链接。

2. 如果这个工程的某几个C文件被修改，那么我们只编译被修改的C文件，并链接目标程序。

3. 如果这个工程的头文件被改变了，那么我们需要编译引用了这几个头文件的C文件，并链接目标程序。

只要我们的Makefile写得够好，所有的这一切，我们只用一个make命令就可以完成，make命令会自动智能地根据当前的文件修改的情况来确定哪些文件需要重编译，从而自己编译所需要的文件和链接目标程序。

GNU make工作时的执行步骤如下：

1. 读入所有的Makefile。

2. 读入被include的其它Makefile。

3. 初始化文件中的变量。

4. 推导隐晦规则，并分析所有规则。

5. 为所有的目标文件创建依赖关系链。

6. 根据依赖关系，决定哪些目标要重新生成。

7. 执行生成命令。

1-5步为第一个阶段，6-7为第二个阶段。第一个阶段中，如果定义的变量被使用了，那么，make会把其展开在使用的位置。但make并不会完全马上展开，make使用的是拖延战术，如果变量出现在依赖关系的规则中，那么仅当这条依赖被决定要使用了，变量才会在其内部展开。下面对Makefile的相关问题进行简单介绍：

Makefile文件由一组依赖关系和规则构成。

**Makefile的依赖关系：**

依赖关系定义了最终应用程序里的每个文件与源文件之间的关系。每个依赖关系由一个目标和一组该目标所依赖的源文件组成。规则则描述了如何通过这些依赖关系创建目标。依赖关系的写法如下：先写目标的名称，后面紧跟着一个冒号，接着是空格或制表符，最后是用空格或制表符隔开的文件列表，如：

```makefile
myapp：main.o my.o
main.o: main.c a.h
my.o: my.c b.h
```

它表示目标myapp依赖于main.o和my.o，而main.o依赖于main.c和a.h，等等。这组依赖关系形成一个层次结构，它显示了源文件之间的关系，如果a.h发生了变化，就需要重新编译main.o。

**Makefile的规则：**

```makefile
arget ... : prerequisites ... 
	command
```

target也就是一个目标文件，可以是Object File，也可以是执行文件。prerequisites就是，要生成那个target所需要的文件或是目标。 command也就是make需要执行的命令。（任意的Shell命令）command一定要以Tab键开始，否者编译器无法识别command 

这是一个文件的依赖关系，也就是说，target这一个或多个的目标文件依赖于prerequisites中的文件，其生成规则定义在command中。说白一点就是说，prerequisites中如果有一个以上的文件比target文件要新的话，command所定义的命令就会被执行。这就是Makefile的规则。也就是Makefile中最核心的内容。

一个简单的Makefile文件：

```makefile
myapp: main.o my.o
	gcc –o myapp main.o my.o
main.o: main.c a.h
	gcc –c main.c 
my.o : my.c b.h
	gcc –c my.c 
clean:
	rm myapp main.o my.o
```

在这个makefile中，目标文件（target）包含：执行文件myapp和中间目标文件（*.o），依赖文件（prerequisites）就是冒号后面的那些 .c 文件和 .h文件。每一个 .o 文件都有一组依赖文件，而这些 .o 文件又是执行文件 myapp的依赖文件。依赖关系的实质上就是说明了目标文件是由哪些文件生成的，换言之，目标文件是哪些文件更新的。 

在定义好依赖关系后，后续的那一行定义了如何生成目标文件的操作系统命令，一定要以一个Tab键作为开头。记住，make并不管命令是怎么工作的，他只管执行所定义的命令。make会比较targets文件和prerequisites文件的修改日期，如果prerequisites文件的日期要比targets文件的日期要新，或者target不存在的话，那么，make就会执行后续定义的命令。 

这里要说明一点的是，clean不是一个文件，它只不过是一个动作名字，有点像C语言中的lable一样，其冒号后什么也没有，那么，make就不会自动去找文件的依赖性，也就不会自动执行其后所定义的命令。要执行其后的命令，就要在make命令后明显得指出这个lable的名字。这样的方法非常有用，我们可以在一个makefile中定义不用的编译或是和编译无关的命令，比如程序的打包，程序的备份，等等。

make是如何工作的

在默认的方式下，也就是我们只输入make命令。那么，

1. make会在当前目录下找名字叫“Makefile”或“makefile”的文件。

2. 如果找到，它会找文件中的第一个目标文件（target），在上面的例子中，他会找到

`myapp`这个文件，并把这个文件作为最终的目标文件。

3. 如果myapp文件不存在，或是myapp所依赖的后面的 .o 文件的文件修改时间要比myapp这个文件新，那么，他就会执行后面所定义的命令来生成myapp这个文件。

4. 如果myapp所依赖的.o文件也存在，那么make会在当前文件中找目标为.o文件的依赖性，如果找到则再根据那一个规则生成.o文件。（这有点像一个堆栈的过程）

5. 当然，你的C文件和H文件是存在的啦，于是make会生成 .o 文件，然后再用 .o 文件生成make的终极任务，也就是执行文件myapp了。

#### 1.2 描述和审核要求

***\*任务描述：\****

1. 创建hello.c文件，编写一个简单的helloworld程序

2. 使用命令行的方法手动编译hello.c文件

3. 学生查询Makefile的相关资料，自主学习Makefile的基本书写规则

4. 编写hello.c文件的Makefile文件，并使用命令生成可执行文件

***\*审核要求：\****

1. 掌握Makefile最基本的书写规则

2. 正确编写源程序以及该源文件的Makefile文件，能正确运行Makefile文件

3. 提交编写的程序代码以及Makefile文件，以及运行结果的截图，并记录在编写Makefile文件时遇到的错误和修改方法，代码思路规范清晰，命名规范。

#### 1.3 参考答案

hello.c

```c
#include <stdio.h>
#include <stdlib.h>

int main()
{
   printf("hello.world!\n");
   return 0;
}
```

makefile

```makefile
hello : hello.o
	cc -o hello hello.o
hello.o : hello.c
	cc -c hello.c -o hello.o
clean :
	rm -f hello.o
```

### 2. 子任务2：Makefile文件的优化

#### 2.1 相关知识

Makefile里主要包含了五个东西：显式规则、隐晦规则、变量定义、文件指示和注释。 

1. 显式规则。显式规则说明了，如何生成一个或多的的目标文件。这是由Makefile的书写者明显指出，要生成的文件，文件的依赖文件，生成的命令。 

2. 隐晦规则。由于我们的make有自动推导的功能，所以隐晦的规则可以让我们比较粗糙地简略地书写Makefile，这是由make所支持的。 

3. 变量的定义。在Makefile中我们要定义一系列的变量，变量一般都是字符串，这个有点你C语言中的宏，当Makefile被执行时，其中的变量都会被扩展到相应的引用位置上。 

4. 文件指示。其包括了三个部分，一个是在一个Makefile中引用另一个Makefile，就像C语言中的include一样；另一个是指根据某些情况指定Makefile中的有效部分，就像C语言中的预编译#if一样；还有就是定义一个多行的命令。有关这一部分的内容，我会在后续的部分中讲述。 

5. 注释。Makefile中只有行注释，和UNIX的Shell脚本一样，其注释是用“#”字符，这个就像C/C++中的“//”一样。如果你要在你的Makefile中使用“#”字符，可以用反斜框进行转义，如：“\#”。 

Makefile的优化：在前面已经接触了Makefile的书写，但是这样写出的Makefile文件会很冗长，而且不易维护和修改，可以使用宏和内部规则来大大简化Makefile的书写。

##### 2.1.1 Makefile中宏的使用

在windows操作系统中进行C语言编程时就使用过宏，其实汇编语言中也有宏，甚至还有Word中的宏病毒。其实，宏就是代表字符串的短名，但是它和字符串有很大的不同。如果文件或者程序中遇到字符串常量或变量时，就会将它当成一种特定类型的常量或变量。如果遇到宏时，就在宏名出现的地方用它代表的字符串来进行代替，将这个字符串作为文件有机的组成部分。这样，当定义了宏之后，如果需要修改宏，只需在宏定义的地方进行修改，而不需在程序中每次出现宏名的地方进行修改。

要在make中使用宏，首先要定义宏，然后在Makefile中引用。之所以这种顺序是因为make处理Makefile时类似于解释执行，如果没有预编译的过程，就不能识别那些没有定义的宏名。宏定义的格式如下：

宏名 赋值符号 宏的值

其中宏名由程序员自己制定，可以使字母数字以及下划线的任意组合。但不能是数字开头，一般习惯用大写，而且为了便于使用和维护，最好使名字具有一定的实际意义。赋值符号可以取一下三种：

= 直接将后面的字符串赋值给宏

：= 将后面的字符串常量赋值给前面的宏

+= 使宏原来的值加上一个空格，再加上后面的字符串，作为新的宏值

**注意：赋值符号前面除了空格之外，不能有制表符或其他分隔符，否则make会把它作为宏名的一部分。**

宏的引用格式有两种：

$(宏名)或者${宏名}

上述Makefile存在的问题是，它假设编译器的名字是gcc，而在其他UNIX系统中，编译器的名字是cc或者c89，如果想将Makefile文件移植到另一个版本的UNIX中，或者在现有系统中使用另一个编译器，为了使其工作，你不得不修改Makefile文件中许多行的内容。

上例中利用宏对Makefile进行优化后结果如下：

```makefile

objects= main.o my.o
CC=gcc

myapp: $(objects)
	$(CC) –o myapp $(objects)
main.o: main.c a.h
	$(CC) –c main.c 
my.o : my.c b.h
	$(CC) –c my.c 
clean:
	rm myapp $(objects)
```

可以在三种地方对宏进行定义：第一是在Makefile中，第二是在Makefile命令行中，第三是在载入环境中。类似于属性的控制，这几种宏的定义也要区分优先级。Make在处理Makefile时，它将先给内定义的宏赋值，再给载入的shell宏赋值，然后给Makefile中的宏赋值，最后才处理make命令行中的宏定义。

##### 2.1.2  Makefile的隐晦规则

GNU的make很强大，它可以自动推导文件以及文件依赖关系后面的命令，于是我们就没必要去在每一个[.o]文件后都写上类似的命令，因为，我们的make会自动识别，并自己推导命令。只要make看到一个[.o]文件，它就会自动的把[.c]文件加在依赖关系中，如果make找到一个whatever.o，那么whatever.c，就会是whatever.o的依赖文件。并且 cc -c whatever.c也会被推导出来，于是，我们的Makefile再也不用写得这么复杂。我们的是新的Makefile又出炉了。

```makefile
objects= main.o my.o
CC=gcc

myapp: $(objects)
	$(CC) –o myapp $(objects)
main.o: a.h
my.o : my.c b.h
clean:
rm myapp $(objects)
```

##### 2.1.3  清空目标文件的规则

每个Makefile中都应该写一个清空目标文件（.o和执行文件）的规则，这不仅便于重编译，也很利于保持文件的清洁。一般的风格都是：

```makefile
clean:
	rm myapp $(objects)
```

更为稳健的做法是：

```makefile
.PHONY : clean

clean :
	rm myapp $(objects)
```

.PHONY意思表示clean是一个“伪目标”，。而在rm命令前面加了一个小减号的意思就是，也许某些文件出现问题，但不要管，继续做后面的事。当然，clean的规则不要放在文件的开头，不然，这就会变成make的默认目标，相信谁也不愿意这样。不成文的规矩是——“clean从来都是放在文件的最后”。

#### 2.2 描述和审核要求

***\*任务描述：\**** 

对之前编写的Makefile文件采用变量和隐晦规则进行优化，能正确运行

***\*审核要求：\****

1. 完成Makefile文件的优化

2. 能正确运行优化后的Makefile文件

3. 提交全部程序代码或者指令文件，代码思路规范清晰，命名规范。

#### 2.3参考答案

$? 当前目标所依赖的文件列表中比当前目标还要新的文件

$@ 当前目标的名字

$< 当前依赖文件的名字

$* 不包括后缀名的当前依赖文件的名字

### 3. 子任务3：同一目录下，多个源码文件的单个Makefile书写

#### 3.1 相关知识

同上述资料

**\*隐含规则\***

GNU make中有一些内置或隐含的规则，这些规则定义了如何从不同的依赖文件建立特定类型的目标。

GNU make支持两种不同类型的隐含规则：

（1）后缀规则，是定义隐含规则的老风格方法。后缀规则定义了将一个一个具有某个后缀的文件转换具有另外一个后缀的文件的方法。每个后缀规则以两个承兑出现的后缀名定义，例如，将.c文件转换为.o文件的后缀规则可以定义为：

```makefile
.c.o:
$(CC) $(CCFLAGS) $(CPPFLAGS) –c –o $@ $<
```

（2）模式规则，这种规则更加通用，因为可以利用这种模式规则定义更加复杂的依赖规则。模式规则看起来更加类似于正则规则，但在目标名称的前面多了一个%号，同时可以用来定义目标和依赖文件之间的关系，例如下面的模式规则定义了如何将任意一个X.c文件转换为X.o文件：

```makefile
%.c:%.o
$(CC) $(CCFLAGS) $(CPPFLAGS) –c –o $@ $<
```

***\*文件引用：\****

在Makefile中使用include关键字可以把别的Makefile包含进来，这很像C语言中的#include，被包含的文件会原模原样的放在当前文件的包含位置。

例如有这样几个Makefile：a.mk b.mk c.mk，还有这样一个文件foo.make，以及一个变量$(bar)，其中包含了e.mk和f.mk，那么下面的语句：

```makefile
include foo.make *.mk $(bar)
```
等价于

```makefile
include foo.mk a.mk b.mk c.mk e.mk f.mk
```

make命令开始时会找寻include所指出的其他Makefile，并把内容安置在当前位置。如果文件没有指定绝对路径或相对路径的话，make首先会在当前目录下寻找，如果当前目录下没找到，那么，make会在下面的几个目录寻找：

1. 如果make执行时，有“--I”或“--include-dir”参数，那么make就会在这个参数指定的目录下寻找

2. 如果目录<prefix>/include(一般是/usr/local/bin或/usr/include)存在的话，make会去找。

如果文件没有找到的话，make会生成一条警告信息，但不会马上出现致命错误，它会继续载入其他的文件，一旦完成Makefile的读取，make会在重试这些没有找到的，或是不能读取的文件，如果还是不行，make才会出现一条致命信息。

**Makefile中的函数**

在Makefile中可以使用函数来处理变量，从而让命令和规则更为灵活和具有智能，函数调用很像变量的使用，也是以‘$’来标示的，函数调用后，函数的返回值可以当做变量来使用。

例如，“wildcard”函数，可以展开成一列所有符合由其参数描述的文件名。文件间以空格间隔，语法如下：

```makefile
$(wildcard PARTERN…)
```

用wildcard函数找出目录中所有的.c文件：sources=$(wildcard *.c)。实际上，GNU make还有许多如字符串处理函数，文件名操作函数等其他函数。

**make的执行**

一般来说，最简单的就是直接在命令行下输入make命令，GNU make找寻默认的Makefile的规则是在当前目录下一次找到三个文件——“GNUmakefile”，“makefile”和“Makefile”。按顺序找这三个文件，一旦找到，就开始读取文件并执行，也可以给make命令指定一个特殊名字的Makefile，要达到这个功能，要求使用make的“-f”或是“--file”参数，例如：make –f hello.makefile

#### 3.2 描述和审核要求

***\*任务描述：\****

在同一目录下创建三个源文件，一个输出helloword，一个输出自己的信息（依赖于helloworld的程序），还有一个头文件，为这三个源文件编写一个Makefile文件，正确执行，输出结果

***\*审核要求：\****

1. 正确完成三个源文件编写

2. 正确完成Makefile文件的编写，正确运行，能输出所要求的结果

3. 提交全部程序代码或者指令文件，代码思路规范清晰，命名规范。

#### 3.3 参考答案


hello.c   

```c
#include "headfile.h" 

int main()
{
	printf("hello.world!\n");
	myfun();
	return 0;
 }
```

myfun.c

```c
#include "headfile.h"

void myfun(){
	printf("hello,myfun\n");
}
```

headfile.h

```c
#include <stdlib.h>
#include <stdio.h>

void myfun();
```

makefile

```makefile
hello : hello.o myfun.o
	gcc hello.o myfun.o  -o  hello
hello.o : hello.c headfile.h
	gcc -c -Wall -g hello.c -o hello.o
myfun.o : myfun.c headfile.h
	gcc -c -Wall -g myfun.c -o myfun.o

clean :
	rm -f *.o hello
```
### 4. 子任务4：多个目录下，多个源码文件的单个Makefile书写

#### 4.1 相关知识

在一些大的工程中,有大量的源文件,我们通常的做法是把这许多的源文件分类,并存放在不同的目录中。所以,当 make 需要去找寻文件的依赖关系时,你可以在文件前加上路径,但最好的方法是把一个路径告诉 make,让 make 在自动去找。

Makefile 文件中的特殊变量“VPATH”就是完成这个功能的,如果没有指明这个变量,make只会在当前的目录中去找寻依赖文件和目标文件。如果定义了这个变量,那么,make 就会在当前目录找不到的情况下,到所指定的目录中去找寻文件了。

```makefile
VPATH = src:../headers
```

上面的的定义指定两个目录,“src”和“../headers”,make 会按照这个顺序进行搜索。目录由“冒号”分隔。

另一个设置文件搜索路径的方法是使用 make 的“vpath”关键字,这不是变量,这是一个 make 的关键字,这和上面提到的那个 VPATH 变量很类似,但是它更为灵活。 它可以指定不同的文件在不同的搜索目录中。这是一个很灵活的功能。 它的使用方法有三种:

1. vpath

为符合模式的文件指定搜索目录。

2. vpath

清除符合模式的文件的搜索目录。

3. vpath

清除所有已被设置好了的文件搜索目录。

vapth 使用方法中的需要包含“%”字符。“%”的意思是匹配零或若干字符,例如,“%.h”表示所有以“.h”结尾的文件。指定了要搜索的文件集,而则指定了的文件集的搜索的目录。例如:

```makefile
vpath %.h ../headers
```

该语句表示,要求 make 在“../headers”目录下搜索所有以“.h”结尾的文件。(如果某文件在当前目录没有找到的话)

我们可以连续地使用 vpath 语句,以指定不同搜索策略。如果连续的 vpath 语句中出现了相同的,或是被重复了的,那么,make 会按照 vpath 语句的先后顺序来执行搜索。如:

```makefile
vpath %.c foo
vpath % blish
vpath %.c bar
```

其表示“.c”结尾的文件,先在“foo”目录,然后是“blish”,最后是“bar”目录。

```makefile
vpath %.c foo:bar
vpath % blish
```

而上面的语句则表示“.c”结尾的文件,先在“foo”目录,然后是“bar”目录,最后才是“blish”目录。

#### 4.2 描述和审核要求

任务描述：

创建一个总目录，创建一个输出自己信息的源文件，以及一个Makefile，再创建两个子目录，一个放置helloworld程序，一个放置头文件，依赖关系同上个任务，编译运行，正确输出结果。

***\*审核要求：\****

1. 正确编译源文件以及Makefile文件

2. 正确编译运行，输出结果

3. 提交相关源文件代码，Makefile文件，以及运行结果的截图的文档，代码思路规范清晰，命名规范。

#### 4.3 参考答案

makefile

```makefile
CFLAGS = -g -Wall -I/home/nfs/zhang/mk/include
SRC = /home/nfs/zhang/mk/src/myfun.c

hello : hello.o myfun.o
	gcc hello.o myfun.o  -o  hello
hello.o : hello.c
	gcc $(CFLAGS) -c hello.c -o hello.o
myfun.o : ./src/myfun.c
	gcc $(CFLAGS) -c $(SRC) -o myfun.o
clean :
	rm -f *.o hello
```

### 5. 子任务5：多个目录下，多个源码文件的多Makefile书写

#### 5.1 相关知识

嵌套执行make

在一些大的工程中，不同模块或是不同功能的源文件放在不同的目录下，可以在每一个目录中都写一个该目录下的Makefile，这有利于Makefile变得更加地简洁，而不至于把所有的东西都写在同一个Makefile中，这项技术对于进行模块编译和分段编译有着非常大的好处。

例如，有一个子目录叫subdir，这个目录下有个Makefile文件指明了这个目录下的变异规则。那么总控的Makefile可以这么写：

```makefile
subsystem：
	cd subdir && $(MAKE)
```

#### 5.2 描述和审核要求

***\*任务描述：\****

创建一个总目录，创建一个输出自己信息的源文件，以及一个Makefile，再创建两个子目录，一个放置helloworld程序，一个放置头文件，每个子目录都有自己的Makefile，依赖关系同上个任务，编译运行，正确输出结果。

***\*审核要求：\****

1.. 正确编译源文件以及Makefile文件

2. 正确编译运行，输出结果

3. 提交相关源文件代码，Makefile文件，以及运行结果的截图的文档，代码思路规范清晰，命名规范。

#### 5.3 参考答案

makefile

```makefile
CFLAGS = -g -Wall -I/home/nfs/zhang/mk/include

hello : hello.o myfun.o
	gcc hello.o myfun.o  -o  hello
hello.o : hello.c
	gcc $(CFLAGS) -c hello.c -o hello.o
myfun.o : ./src/myfun.c
	d src;make 
clean :
	rm -f *.o hello
	cd src;make clean
```

src/makefile

```makefile
CFLAGS = -g -Wall -I/home/nfs/zhang/mk/include

myfun.o : myfun.c
	gcc $(CFLAGS) -c myfun.c -o ../myfun.o
clean :
	rm -f *.o
```


### 6. 子任务6：分析linux 0.11中kernel部分的Makefile文件 

#### 6.1 相关知识

**GNU make的主要预定义变量**

GNU make 有许多预定义的变量，这些变量具有特殊的含义，可在规则中使用。以下给出了一些主要的预定义变量，除这些变量外，GNU make 还将所有的环境变量作为自己的预定义变量。

$@ ——表示规则中的目标文件集。在模式规则中，如果有多个目标，那么，"$@"就是匹配于目标中模式定义的集合。

$% ——仅当目标是函数库文件中，表示规则中的目标成员名。例如，如果一个目标是"foo.a(bar.o)"，那么，"$%"就是"bar.o"，"$@"就是"foo.a"。如果目标不是函数库文件（Unix下是[.a]，Windows下是[.lib]），那么，其值为空。

$< ——依赖目标中的第一个目标名字。如果依赖目标是以模式（即"%"）定义的，那么"$<"将是符合模式的一系列的文件集。注意，其是一个一个取出来的。

$? ——所有比目标新的依赖目标的集合。以空格分隔。

$^ ——所有的依赖目标的集合。以空格分隔。如果在依赖目标中有多个重复的，那个这个变量会去除重复的依赖目标，只保留一份。

$+ ——这个变量很像"$^"，也是所有依赖目标的集合。只是它不去除重复的依赖目标。

**命令的变量。**

AR  函数库打包程序。默认命令是“ar”。 

AS  汇编语言编译程序。默认命令是“as”。

CC  C语言编译程序。默认命令是“cc”。

CXX  C++语言编译程序。默认命令是“g++”。

CO  从 RCS文件中扩展文件程序。默认命令是“co”。

CPP  C程序的预处理器（输出是标准输出设备）。默认命令是“$(CC) –E”。

FC  Fortran 和 Ratfor 的编译器和预处理程序。默认命令是“f77”。

GET  从SCCS文件中扩展文件的程序。默认命令是“get”。 

LEX  Lex方法分析器程序（针对于C或Ratfor）。默认命令是“lex”。

PC   Pascal语言编译程序。默认命令是“pc”。

YACC   Yacc文法分析器（针对于C程序）。默认命令是“yacc”。

YACCR   Yacc文法分析器（针对于Ratfor程序）。默认命令是“yacc –r”。

MAKEINFO   转换Texinfo源文件（.texi）到Info文件程序。默认命令是“makeinfo”。

TEX   从TeX源文件创建TeX DVI文件的程序。默认命令是“tex”。

TEXI2DVI   从Texinfo源文件创建军TeX DVI 文件的程序。默认命令是“texi2dvi”。

WEAVE   转换Web到TeX的程序。默认命令是“weave”。

CWEAVE   转换C Web 到 TeX的程序。默认命令是“cweave”。

TANGLE   转换Web到Pascal语言的程序。默认命令是“tangle”。

CTANGLE   转换C Web 到 C。默认命令是“ctangle”。

RM  删除文件命令。默认命令是“rm –f”。

**命令参数变量：**

下面的这些变量都是相关上面的命令的参数。如果没有指明其默认值，那么其默认值都是空。

ARFLAGS   函数库打包程序AR命令的参数。默认值是“rv”。

ASFLAGS   汇编语言编译器参数。（当明显地调用“.s”或“.S”文件时）。 

CFLAGS    C语言编译器参数。

CXXFLAGS   C++语言编译器参数。

COFLAGS   RCS命令参数。 

CPPFLAGS   C预处理器参数。（ C 和 Fortran 编译器也会用到）。

FFLAGS   Fortran语言编译器参数。

GFLAGS   SCCS “get”程序参数。

LDFLAGS   链接器参数。（如：“ld”）

LFLAGS   Lex文法分析器参数。

PFLAGS   Pascal语言编译器参数。

RFLAGS   Ratfor 程序的Fortran 编译器参数。

YFLAGS   Yacc文法分析器参数。

#### 6.2 描述和审核要求

***\*任务描述：\****

利用所学的有关Makefile的知识，阅读linux0.11部分中的kernel中的Makefile文件，并对其进行分析，并到讲台上来讲解。

***\*审核要求：\****

1. 正确分析kernel部分的Makefile文件

2. 到讲台上讲解自己的分析结果

3. 提交分析文档

