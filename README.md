# 《RISC-V openEuler应用编程技术》

## 课程介绍

课程名称：RISC-V openEuler应用编程技术

学时：待定*

适用专业：计算机科学、计算机工程、信息安全、网络工程

先修课程：计算机组成原理/计算机导论与 C 语言程序设计/数据结构与算法分析

教学目标：使学生具备熟练的openEuler等Linux系统环境下应用程序设计、编写、移植、调试、运行跟踪的综合能力。

## 课程大纲

- 第一章 RISC-V openEuler简介
- 第二章 openEuler下C语言开发与调试
- 第三章 Shell
- 第四章 程序库
- 第五章 RISC-V程序构建
- 第六章 进程与线程
- 第七章 文件系统应用
- 第八章 网络应用编程
- 第九章 安全应用编程
- 第十章 RISC-V处理器加速编程
- 第十一章 RISC-V包移植

## 课表

| 序号 | 内容                                                         |
| ---- | :----------------------------------------------------------- |
| 1 | [第一章 第一讲 RISC-V ISA介绍](./chapter1/class1) |
| 2| [第一章 第二讲 Linux和openEuler发展史](./chapter1/class2)  |
| 3 | [第一章 第三讲 RISC-V嵌入式开发简介](./chapter1/class3)  |
|  4| [第一章 实验一 使用QEMU安装RISC-V openEuler](./chapter1/lab1) |
| 5 | [第一章 实验二 使用D1 开发板安装openEuler RISC-V（可选）](./chapter1/lab2) |
| 6 | [第一章 实验三 使用Visionfive开发板安装RISC-V（可选）](./chapter1/lab3)  |
|  7| [第一章 实验四 使用Unmatched开发板安装RISC-V（可选）](./chapter1/lab4)  |
| 8 | [第一章 实验五 RISC-V交叉编译](./chapter1/lab5)  |
| 9 | [第一章 第一讲 VIM](./chapter2/class1) |
| 10 | [第二章 第二讲 GCC](./chapter2/class2) |
| 11 | [第二章 第三讲 Makefile和CMake](./chapter2/class3) |
|  12 | [第二章 第四讲 程序调试](./chapter2/class4) |
|  13 | [第二章 第五讲 Git](./chapter2/class5) |
|  13 | [第二章 实验一 VIM](./chapter2/lab1) |
|  14 | [第二章 实验二 Makefile的使用](./chapter2/lab2) |
